#include <SDL.h>

#include "demo.h"

bool hasQuit;
SDL_Window *sdlWindow;
SDL_Renderer *sdlRenderer;
SDL_Event sdlEvent;
SDL_Texture *sdlTexture;

int main(int argc, char * arg[])
{
    SDL_Init(SDL_INIT_EVERYTHING);
    sdlWindow = SDL_CreateWindow("Window", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, demo_get_screen_width(), demo_get_screen_height(), SDL_WINDOW_OPENGL);
    sdlRenderer = SDL_CreateRenderer(sdlWindow, -1, SDL_RENDERER_ACCELERATED);
    sdlTexture = SDL_CreateTexture(sdlRenderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STREAMING, demo_get_screen_width(), demo_get_screen_height());
    
    uint32_t *pixels = (uint32_t*)(malloc(demo_get_screen_width() * demo_get_screen_height() * sizeof(uint32_t)));
    demo_init(SDL_GetTicks());
    demo_set_pixels(pixels);

    while (!hasQuit) {
        while (SDL_PollEvent(&sdlEvent)) {
            if (sdlEvent.type == SDL_QUIT) {
                hasQuit = true;
            }
        }

        demo_render(SDL_GetTicks());
        demo_blit();

        SDL_UpdateTexture(sdlTexture, NULL, pixels, demo_get_screen_width() * sizeof (uint32_t));
        SDL_RenderClear(sdlRenderer);
        SDL_RenderCopy(sdlRenderer, sdlTexture, NULL, NULL);
        SDL_RenderPresent(sdlRenderer);
    }
   
    SDL_DestroyRenderer(sdlRenderer);
    SDL_DestroyWindow(sdlWindow);
    SDL_Quit();
    
    return 0;
}
