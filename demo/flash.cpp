#include <AS3/AS3.h>
#include <Flash++.h>
extern "C" { 
#include "demo.h"
}

using namespace AS3::ui;

flash::display::BitmapData flashBitmapData; 
flash::text::TextField flashText;
char fpsBuffer[200];
uint32_t *pixels;

static var onEnterFrame(void *arg, var args)
{

    flash::events::Event event = flash::events::Event(args[0]);
    flash::utils::ByteArray ram = internal::get_ram();

    demo_render(flash::utils::getTimer());
    demo_blit();

    flashBitmapData->setPixels(flashBitmapData->rect, ram, pixels);

    sprintf(fpsBuffer, "%d", demo_get_fps());
    flashText->text = internal::new_String(fpsBuffer);
    return internal::_undefined;
}

int main()
{
    flash::display::Stage stage = internal::get_Stage();

    flashBitmapData = flash::display::BitmapData::_new(demo_get_screen_width(), demo_get_screen_height(), false);
    flash::display::Bitmap bitmap = flash::display::Bitmap::_new(flashBitmapData);

    stage->addChild(bitmap);

    flashText = flash::text::TextField::_new();
    flashText->background= true;
    flashText->backgroundColor = 0xFFFFFFFF;
    flashText->height = 30;
    flashText->width = 50;
    stage->addChild(flashText);

    stage->addEventListener("enterFrame", Function::_new(&onEnterFrame, NULL));

    demo_init(flash::utils::getTimer());

    pixels = new uint32_t[demo_get_screen_width() * demo_get_screen_height()];
    demo_set_pixels((uint32_t*)pixels);

    AS3_GoAsync();

    return 0;
}
