#include <SDL/SDL.h>
#include <math.h>
#include <emscripten.h>
#include "demo.h"

SDL_Surface *screen;

void loop() {
    SDL_Event event;

    demo_render(SDL_GetTicks());

    SDL_LockSurface(screen);
    demo_blit();
    SDL_UnlockSurface(screen);
    SDL_Flip(screen);
}

int main(int argc, char **argv) {

    SDL_Init(SDL_INIT_VIDEO);
    screen = SDL_SetVideoMode(demo_get_screen_width(), demo_get_screen_height(), 32, SDL_SWSURFACE);

    demo_init(SDL_GetTicks());
    demo_set_pixels((uint32_t*)screen->pixels);

    emscripten_set_main_loop(loop, 0, 1);

    return 0;
}
