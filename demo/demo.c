#include "../src/sgl.h"

#include <stdio.h>
#include <math.h>

#ifdef __cplusplus
extern "C" {
#endif

int screen_width = 640;
int screen_height = 480;

uint32_t* demo_pixels;
uint32_t texture_src[64*64];
sgl_texture texture;
float angle = 1.5f;
uint32_t fps_ticks;
int fps;
int printed_fps;
uint32_t dt;
uint32_t last_ticks;

void put_pixel(int x, int y, uint32_t color) {
    demo_pixels[x + y * screen_width] = color;
}

float meshVb[] = {
    -1.7f, -1, 1.7f, 1, 0, 2,
    1.7f, -1, 1.7f, 1, 2, 2,
    0, 1, 0, 1, 1, 0,
    -1.7f, -1, -1.7f, 1, 0, 2,
    1.7f, -1, -1.7f, 1, 2, 2,
};

int meshIb[] = {
    0, 1, 2,
    3, 4, 2
};


void demo_init(uint32_t ticks) {
    last_ticks = fps_ticks = ticks;
    printed_fps = 0;

    for (int i = 0; i < 64; ++i) {
        for (int j = 0; j < 64; ++j) {
            texture_src[i + j*64] = (((i >> 3)^(j >> 3))&1) ? 0xFF005500 : 0xFF00ff00;
        }
    }

    sgl_set_framebuffer_size(screen_width, screen_height);
    sgl_set_default_shaders();

    sgl_mtx4_projection(60, 3.f/4.f, 0.1f, 140.0f, sgl_get_proj_mtx());
    sgl_mtx4_identity(sgl_get_view_mtx());

    sgl_set_vertex_buffer(meshVb, 6, 6);
    sgl_set_index_buffer(meshIb, 6);

    sgl_gen_texture(texture_src, 64, 64, &texture);
    sgl_bind_texture(0, &texture);
    
    sgl_enable(SGL_PERSPECTIVE_CORRECTION);
}

void demo_render(uint32_t ticks) {
    dt = ticks - last_ticks;
    last_ticks += dt;
    
    fps++;
    if (last_ticks - fps_ticks >= 1000) {
        fps_ticks = last_ticks;
        printf("FPS: %d\n", fps);
        printed_fps = fps;
        fps=0;
    }

    angle += 0.005;
    sgl_mtx4_identity(sgl_get_model_mtx());
    float axis[3] = {10, 4, 0 };
    float l = sqrtf(axis[0]*axis[0] + axis[1]*axis[1] +axis[2]*axis[2]);
    axis[0] /= l; axis[1] /= l; axis[2] /= l;
    sgl_mtx4_axis_rot(axis[0], axis[1], axis[2], angle, sgl_get_model_mtx());
    sgl_mtx4_translate(0, 0, 5, sgl_get_model_mtx());

    sgl_clear_framebuffer(0);       
    sgl_clear_depth(1000);          
    sgl_render_buffer();
}

void demo_blit(void) {
    sgl_draw_framebuffer(put_pixel);
}

int demo_get_screen_width(void) {
    return screen_width;
}

int demo_get_screen_height(void) {
    return screen_height;
}

void demo_set_pixels(uint32_t *pixels) {
    demo_pixels = pixels;
}

int demo_get_fps(void) {
    return printed_fps;
}

#ifdef __cplusplus
}
#endif
