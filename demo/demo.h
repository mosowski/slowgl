
void demo_init(uint32_t ticks);
void demo_render(uint32_t ticks);
void demo_blit(void);
int demo_get_screen_width(void);
int demo_get_screen_height(void);
void demo_set_pixels(uint32_t *pixels);
int demo_get_fps(void);
