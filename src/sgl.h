#ifndef sgl_sgl_h
#define sgl_sgl_h

#ifdef __cplusplus
extern "C" {
#endif
    
#include <stdint.h>

#define MAX_NUM_VERTICES 1000
#define NUM_MAX_FRAGMENT_ATTRIBUTES 80
#define NUM_MAX_ACTIVE_TEXTURES 4
    
#define SGL_PERSPECTIVE_CORRECTION 0
#define SGL_NUM_FLAGS 1

typedef struct {
    uint32_t *pixels;
    float *depth;
    int width;
    int height;
} sgl_framebuffer;

typedef struct {
    int width;
    int height;
    int width_mask;
    int height_mask;
    int num_mipmaps;
    uint32_t *pixels;
} sgl_texture;

typedef struct {
    int num_vertex_attribs;
    int num_out_vertex_attribs;
    int num_vertices;
    float *vertex_buffer;
    void (*vertex_shader)(float*,float*);
    float *out_vertex_data;

    int *index_buffer;
    int num_indices;

    void (*fragment_shader)(float*,uint32_t*,float*);
    
    int flags[SGL_NUM_FLAGS];

    float model_mtx[16];
    float view_mtx[16];
    float proj_mtx[16];
    float modelview_mtx[16];
    float modelviewproj_mtx[16];
    sgl_texture *active_textures[NUM_MAX_ACTIVE_TEXTURES];
} sgl_renderstate;

typedef struct {
    float x;
    float y;
    float z;
    float w;
    float u;
    float v;
} sgl_vertex_input;

typedef struct {
    float x;
    float y;
    float z;
    float w;
    float u;
    float v;
} sgl_vertex_output;

typedef sgl_vertex_output sgl_fragment_input;

void sgl_mtx4_copy(float *from, float *to);
void sgl_mtx4_mul_mtx4(float *a, float *b, float *out);
void sgl_mtx4_translate(float x, float y, float z, float *out);
void sgl_mtx4_axis_rot(float x, float y, float z, float a, float *out);
void sgl_mtx4_mul_vec4(float *m, float *v, float *out);
void sgl_mtx4_identity(float *m);
void sgl_mtx4_projection(float fov, float ratio, float near, float far, float *out);

float sgl_vec2_det(float *a, float *b, float *c);

uint32_t sgl_color_mult(uint32_t color, float w);

void sgl_gen_texture(uint32_t *src, int width, int height, sgl_texture *texture);

void sgl_set_framebuffer_size(int width, int height);
void sgl_draw_framebuffer(void (*put_pixel)(int,int,uint32_t));
void sgl_clear_framebuffer(uint32_t color);
void sgl_clear_depth(float depth);
void sgl_set_vertex_buffer(float *vb, int num_vertices, int num_vertex_attribs);
void sgl_set_vertex_shader(void (*vs)(float*,float*), int num_out_vertex_attribs);
void sgl_set_index_buffer(int *index_buffer, int num_indices);
void sgl_set_fragment_shader(void (*fragment_shader)(float*,uint32_t*,float*));
float *sgl_get_view_mtx(void);
float *sgl_get_proj_mtx(void);
float *sgl_get_model_mtx(void);
void sgl_enable(int flag_id);
void sgl_disable(int flag_id);
void sgl_toggle(int flag_id);
void sgl_bind_texture(int id, sgl_texture *texture);
void sgl_concat_matrices(void);
void sgl_rasterize_triangle(float *va, float *vb, float *vc);
void sgl_process_vertex_data(void);
void sgl_render_buffer(void);
uint32_t sgl_texture_sample_lerp(int id, float u, float v);
uint32_t sgl_texture_sample_near(int id, float u, float v);

void sgl_set_default_shaders(void);
void sgl_vertex_shader(float *in, float *out);
void sgl_fragment_shader(float *in, uint32_t *out_color, float *out_depth);

#ifdef __cplusplus
}
#endif
        
#endif
