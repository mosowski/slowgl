#include "sgl.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

static sgl_framebuffer framebuffer;
static sgl_renderstate state;

void sgl_mtx4_print(float *m) {
    for (int j = 0; j < 4; ++j) {
        printf("[ ");
        for (int i = 0; i < 4; ++i) {
            printf("%f(%d) ", m[j*4 + i], j*4+i);
        }
        printf("]\n");
    }
}

void sgl_mtx4_copy(float *from, float *to) {
    for (int i = 0; i < 16; ++i) {
        to[i] = from[i];
    }
}

void sgl_mtx4_mul_mtx4(float *a, float *b, float *out) {
    for (int j = 0; j < 4; ++j) {
        for (int i = 0; i < 4; ++i) {
            out[j*4+i] = a[j*4]*b[i] + a[j*4+1]*b[i+4] + a[j*4+2]*b[i+8] + a[j*4+3]*b[i+12];
        }
    }
}

void sgl_mtx4_mul_vec4(float *m, float *v, float *out) {
    for (int i = 0; i < 4; ++i) {
        out[i] = m[i*4]*v[0] + m[i*4+1]*v[1] + m[i*4+2]*v[2] + m[i*4+3]*v[3];
    }
}

void sgl_mtx4_translate(float x, float y, float z, float *out) {
    out[0] += out[12]*x;   out[1] += out[13]*x;   out[2] += out[14]*x;   out[3] += out[15]*x;
    out[4] += out[12]*y;   out[5] += out[13]*y;   out[6] += out[14]*y;   out[7] += out[15]*y;
    out[8] += out[12]*z;   out[9] += out[13]*z;   out[10]+= out[14]*z;   out[11]+= out[15]*z;
}

void sgl_mtx4_axis_rot(float x, float y, float z, float a, float *out) {
    float s = sinf(a);
    float c = cosf(a);
    float t = 1.0f - c;

    out[0] = c + x*x*t;
    out[1] = x*y*t - z*s;
    out[2] = x*z*t + y*s;
    out[4] = x*y*t + z*s;
    out[5] = c + y*y*t;
    out[6] = y*z*t - x*s;
    out[8] = x*z*t - y*s;
    out[9] = y*z*t + x*s;
    out[10] = c + z*z*t;
    out[3] = out[7] = out[11] = out[12] = out[13] = out[14] = 0;
    out[15] = 1;
}

void sgl_mtx4_identity(float *m) {
    for (int i = 0; i < 16; ++i) {
        m[i] = 0;
    }
    for (int i = 0; i < 4; ++i) {
        m[i*5] = 1;
    }
}

void sgl_mtx4_projection(float fov, float ratio, float near, float far, float *out) {
    float s = 1.0f / tanf(fov * 0.5f * M_PI / 180.0f);
    sgl_mtx4_identity(out);
    out[0] = s * ratio;
    out[5] = s;
    out[10] = (far + near) / (far - near);
    out[11] = 2 * (far * near) / (far - near);
    out[14] = 1;
    out[15] = 0;
}

float sgl_vec2_det(float *a, float *b, float *c) {
    return (b[0]-a[0])*(c[1]-a[1]) - (b[1]*a[1])*(c[0]-a[0]);
}

uint32_t sgl_color_mult(uint32_t color, float w) {
    uint32_t wi = w * 255;
    return (color & 0xFF) * wi >> 8
        | ((color & 0xFF00) * wi >> 8 & 0xFF00)
        | ((color & 0xFF0000) * wi >> 8 & 0xFF0000)
        | ((color & 0xFF000000) >> 8 * wi & 0xFF000000);
}


void sgl_gen_texture(uint32_t *src, int width, int height, sgl_texture *texture) {
    if (texture->pixels != NULL) {
        free(texture->pixels);
    }
    texture->width = width;
    texture->height = height;
    texture->width_mask = width - 1;
    texture->height_mask = height - 1;
    texture->pixels = (uint32_t*)(malloc(sizeof(uint32_t) * width * height));
    texture->num_mipmaps = 1;
    memcpy(texture->pixels, src, sizeof(uint32_t) * width * height);
}

void sgl_set_framebuffer_size(int width, int height) {
    if (framebuffer.pixels != NULL) {
        free(framebuffer.pixels);
        free(framebuffer.depth);
    }
    framebuffer.pixels = (uint32_t*)(malloc(sizeof(uint32_t) * width * height));
    framebuffer.depth = (float*)(malloc(sizeof(float) * width * height));
    framebuffer.width = width;
    framebuffer.height = height;
}

void sgl_draw_framebuffer(void (*put_pixel)(int,int,uint32_t)) {
    for (int j = 0; j < framebuffer.height; ++j) {
        for (int i = 0; i < framebuffer.width; ++i) {
            put_pixel(i, j, framebuffer.pixels[i + j * framebuffer.width]);
        }
    }
}

void sgl_clear_framebuffer(uint32_t color) {
    for (int j = 0; j < framebuffer.height; ++j) {
        for (int i = 0; i < framebuffer.width; ++i) {
            framebuffer.pixels[i + j * framebuffer.width] = color;
        }
    }
}

void sgl_clear_depth(float depth) {
    for (int j = 0; j < framebuffer.height; ++j) {
        for (int i = 0; i < framebuffer.width; ++i) {
            framebuffer.depth[i + j * framebuffer.width] = depth;
        }
    }
}

void sgl_set_vertex_buffer(float *vb, int num_vertices, int num_vertex_attribs) {
    state.vertex_buffer = vb;
    state.num_vertices = num_vertices;
    state.num_vertex_attribs = num_vertex_attribs;
}

void sgl_set_vertex_shader(void (*vs)(float*,float*), int num_out_vertex_attribs) {
    if (num_out_vertex_attribs != state.num_out_vertex_attribs) {
        state.num_out_vertex_attribs = num_out_vertex_attribs;
        if (state.out_vertex_data != NULL) {
            free(state.out_vertex_data);
        }
        state.out_vertex_data = (float*)(malloc(sizeof(float) * num_out_vertex_attribs * MAX_NUM_VERTICES));
    }
    state.vertex_shader = vs;
}

void sgl_set_index_buffer(int *index_buffer, int num_indices) {
    state.index_buffer = index_buffer;
    state.num_indices = num_indices;
}

void sgl_set_fragment_shader(void (*fragment_shader)(float*,uint32_t*,float*)) {
    state.fragment_shader = fragment_shader;
}

float *sgl_get_view_mtx(void) {
    return state.view_mtx;
}

float *sgl_get_proj_mtx(void) {
    return state.proj_mtx;
}

float *sgl_get_model_mtx(void) {
    return state.model_mtx;
}

void sgl_enable(int flag_id) {
    state.flags[flag_id] = 1;
}

void sgl_disable(int flag_id) {
    state.flags[flag_id] = 0;
}

void sgl_toggle(int flag_id) {
    state.flags[flag_id] ^= 1;
}

void sgl_bind_texture(int id, sgl_texture *texture) {
    state.active_textures[id] = texture;
}

void sgl_concat_matrices(void) {
    sgl_mtx4_mul_mtx4(state.view_mtx, state.model_mtx, state.modelview_mtx);
    sgl_mtx4_mul_mtx4(state.proj_mtx, state.modelview_mtx, state.modelviewproj_mtx);
}

void sgl_process_vertex_data(void) {
    for (int i = 0; i < state.num_vertices; ++i) {
        float *in_vertex_data = &state.vertex_buffer[i * state.num_vertex_attribs];
        float *out_vertex_data = &state.out_vertex_data[i * state.num_out_vertex_attribs];
        state.vertex_shader(in_vertex_data, out_vertex_data);
    }
}

void sgl_rasterize_triangle(float *v0, float *v1, float *v2) {
    float in_interpolated[NUM_MAX_FRAGMENT_ATTRIBUTES];
    
    if ((v1[0]-v0[0])*(v2[1]-v1[1])- (v2[0]-v1[0])*(v1[1]-v0[1]) < 0) {
        float *tmp = v0; v0 = v1; v1 = tmp;
    }

    float min_x = v0[0] < v1[0] ? v0[0] : v1[0];
    min_x = v2[0] < min_x ? v2[0] : min_x;
    float max_x = v0[0] > v1[0] ? v0[0] : v1[0];
    max_x = v2[0] > max_x ? v2[0] : max_x;
    float min_y = v0[1] < v1[1] ? v0[1] : v1[1];
    min_y = v2[1] < min_y ? v2[1] : min_y;
    float max_y = v0[1] > v1[1] ? v0[1] : v1[1];
    max_y = v2[1] > max_y ? v2[1] : max_y;
    min_x = min_x < 0 ? 0 : min_x;
    max_x = max_x >= framebuffer.width ? (framebuffer.width - 1) : max_x;
    min_y = min_y < 0 ? 0 : min_y;
    max_y = max_y >= framebuffer.height ? (framebuffer.height - 1) : max_y;

    int step = 16;
    int step_shift = 4;
    int imin_x = min_x * step;
    int imax_x = max_x * step;
    int imin_y = min_y * step;
    int imax_y = max_y * step;

    int v0x = v0[0] * step;
    int v0y = v0[1] * step;
    int v1x = v1[0] * step;
    int v1y = v1[1] * step;
    int v2x = v2[0] * step;
    int v2y = v2[1] * step;

    int e0x = v1x-v0x;
    int e0y = v1y-v0y;
    int e1x = v2x-v1x;
    int e1y = v2y-v1y;
    int e2x = v0x-v2x;
    int e2y = v0y-v2y;
    
    int area = (v0x-v2x)*(v1y-v2y) - (v0y-v2y)*(v1x-v2x);
    float inv_area = 1.0f / area;
    float w0, w1, w2;
    int iw0, iw1, iw2;
    float v0_zinv = 1.0f/v0[3];
    float v1_zinv = 1.0f/v1[3];
    float v2_zinv = 1.0f/v2[3];
    float z_inv;
    
    int frag_x;
    int frag_y;
    int frag_offset;
    
    float persp_v0[NUM_MAX_FRAGMENT_ATTRIBUTES];
    float persp_v1[NUM_MAX_FRAGMENT_ATTRIBUTES];
    float persp_v2[NUM_MAX_FRAGMENT_ATTRIBUTES];
    
    if (state.flags[SGL_PERSPECTIVE_CORRECTION]) {
        for (int k = 0; k < state.num_out_vertex_attribs; ++k) {
            persp_v0[k] = v0[k] * v0_zinv;
            persp_v1[k] = v1[k] * v1_zinv;
            persp_v2[k] = v2[k] * v2_zinv;
        }
    }

    for (int j = imin_y+step/2; j <= imax_y; j+= step) {
        for (int i = imin_x+step/2; i <= imax_x; i += step) {
            iw0 = e1x*(j-v1y) - e1y*(i-v1x);
            iw1 = e2x*(j-v2y) - e2y*(i-v2x);
            iw2 = e0x*(j-v0y) - e0y*(i-v0x);

            if ((iw0 | iw1 | iw2) >= 0) {
                w0 = iw0 * inv_area;
                w1 = iw1 * inv_area;
                w2 = iw2 * inv_area;
                frag_x = i >> step_shift;
                frag_y = j >> step_shift;
                
                if (state.flags[SGL_PERSPECTIVE_CORRECTION]) {
                    z_inv = 1.0f/ (w0*v0_zinv + w1*v1_zinv + w2*v2_zinv);
                    for (int k = 0; k < state.num_out_vertex_attribs; ++k) {
                        in_interpolated[k] = (w0*persp_v0[k] + w1*persp_v1[k] + w2*persp_v2[k]) * z_inv;
                    }
                } else {
                    for (int k = 0; k < state.num_out_vertex_attribs; ++k) {
                        in_interpolated[k] = w0*v0[k] + w1*v1[k] + w2*v2[k];
                    }
                }
                frag_offset = frag_x + frag_y * framebuffer.width;
                state.fragment_shader(in_interpolated, &framebuffer.pixels[frag_offset], &framebuffer.depth[frag_offset]);
            }
        }
    }
}

void sgl_render_buffer(void) {
    sgl_concat_matrices();
    sgl_process_vertex_data();

    for (int i = 0; i < state.num_indices; i += 3) {
        sgl_rasterize_triangle(
            &state.out_vertex_data[state.index_buffer[i] * state.num_out_vertex_attribs],
            &state.out_vertex_data[state.index_buffer[i + 1] * state.num_out_vertex_attribs],
            &state.out_vertex_data[state.index_buffer[i + 2] * state.num_out_vertex_attribs]
        );
    }
}

static inline uint32_t sgl_texture_sample(sgl_texture *texture, int u, int v) {
    return texture->pixels[(u & texture->width_mask) + (v & texture->height_mask)*texture->width];
}

uint32_t sgl_texture_sample_lerp(int id, float u, float v) {
    sgl_texture *texture = state.active_textures[id];
    u *= texture->width;
    v *= texture->height;
    int ui = floorf(u);
    int vi = floorf(v);
    float uf = u - ui;
    float vf = v - vi;
    uint32_t tl = sgl_texture_sample(texture, ui, vi);
    uint32_t tr = sgl_texture_sample(texture, ui + 1, vi);
    uint32_t bl = sgl_texture_sample(texture, ui, vi + 1);
    uint32_t br = sgl_texture_sample(texture, ui + 1, vi + 1);
    uint32_t colt = sgl_color_mult(tl, 1 - uf) + sgl_color_mult(tr, uf);
    uint32_t colb = sgl_color_mult(bl, 1 - uf) + sgl_color_mult(br, uf);
    return sgl_color_mult(colt, 1 - vf) + sgl_color_mult(colb, vf);
}

uint32_t sgl_texture_sample_near(int id, float u, float v) {
    sgl_texture *texture = state.active_textures[id];
    return sgl_texture_sample(texture, u * texture->width , v * texture->height);
}

void sgl_set_default_shaders(void) {
    sgl_set_vertex_shader(sgl_vertex_shader, 6);
    sgl_set_fragment_shader(sgl_fragment_shader);
}

void sgl_vertex_shader(float *fl_in, float *fl_out) {
    sgl_vertex_input *in = (sgl_vertex_input*)fl_in;
    sgl_vertex_output *out = (sgl_vertex_output*)fl_out;
    sgl_mtx4_mul_vec4(state.modelviewproj_mtx, fl_in, fl_out);
    out->x = (out->x/out->w + 1) * framebuffer.width * 0.5f;
    out->y = (-out->y/out->w + 1) * framebuffer.height * 0.5f;

    out->u = in->u;
    out->v = in->v;
}

void sgl_fragment_shader(float *fl_in, uint32_t *out_color, float *out_depth) {
    sgl_fragment_input *in = (sgl_fragment_input*)fl_in;
    if (out_depth[0] > in->z) {
        float fog = in->z < 7.5f ? 1.0f - in->z / 7.5f : 0;
        out_color[0] = sgl_color_mult(sgl_texture_sample_lerp(0, in->u, in->v), fog);
        out_depth[0] = in->z;
    }
}

